(ns cemerick.cljs.macro)

(def ^:dynamic *clj* false)
(def ^:dynamic *cljs* false)

(defmacro defportable
  [name args & body]
  `(defmacro ~name ~args
     (binding [*clj* (if (thread-bound? #'*clj*)
                       *clj*
                       (not (boolean (:ns ~'&env))))
               *cljs* (if (thread-bound? #'*cljs*)
                        *cljs*
                        (boolean (:ns ~'&env)))]
       ~@body)))
