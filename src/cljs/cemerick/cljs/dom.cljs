(ns ^{:doc "Extending DOM types to core cljs protocols"}
  cemerick.cljs.dom)

; was apparently never a standard, not in latest mozillas, maybe restore later
#_
(extend-type js/NamedNodeMap
  ISequential
  ISeqable
  (-seq
    [this]
    (when (> (alength this) 0)
      (map #(let [a (nth this %)]
              [(.-name a) (.-value a)])
           (range (count this)))))
  
  ILookup
  (-lookup
    [this k]
    (-lookup this k nil)
    (when-let [a (aget this k)]
      (.-value a)))
  (-lookup
    [this k not-found]
    (if-let [a (aget this k)]
      (.-value a)
      not-found))
  
  ICounted
  (-count [this] (alength this))
  
  IIndexed
  (-nth
    [this n]
    (when (< n (count this))
      (aget this n)))
  (-nth
    [this n not-found]
    (if (< n (count this))
      (aget this n)
      (if (undefined? not-found)
        nil
        not-found))))

(extend-type js/NodeList
  ISequential
  ISeqable
  (-seq
    [this]
    (when (> (alength this) 0)
      (map #(aget this %)
           (range (count this)))))
  
  ICounted
  (-count [this] (alength this))
  
  IIndexed
  (-nth
    [this n]
    (when (< n (count this))
      (aget this n)))
  (-nth
    [this n not-found]
    (if (< n (count this))
      (aget this n)
      (if (undefined? not-found)
        nil
        not-found))))
